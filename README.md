# Swarm Based Clustering #

This application implements ant colony optimization and particle swarm optimization to perform unsupervised clustering.

Baseline clustering algorithms, K-Means, DBSCAN, and Competitive Learning are also implemented. 

Local datasets are included. The datasets consist of artificial datasets, as well as datasets from the 
UCI machine learning repository.
